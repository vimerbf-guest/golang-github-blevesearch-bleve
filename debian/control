Source: golang-github-blevesearch-bleve
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-blevesearch-go-porterstemmer-dev,
               golang-github-blevesearch-segment-dev,
               golang-github-boltdb-bolt-dev,
               golang-github-couchbase-moss-dev,
               golang-github-seiflotfy-cuckoofilter-dev,
               golang-github-spf13-cobra-dev,
               golang-github-steveyen-gtreap-dev,
               golang-github-willf-bitset-dev,
               golang-golang-x-net-dev,
               golang-golang-x-text-dev,
               golang-github-syndtr-goleveldb-dev,
               golang-goprotobuf-dev,
               golang-github-rcrowley-go-metrics-dev
Standards-Version: 4.1.1
Homepage: https://github.com/blevesearch/bleve
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-blevesearch-bleve
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-blevesearch-bleve.git
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: github.com/blevesearch/bleve

Package: golang-github-blevesearch-bleve-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-blevesearch-go-porterstemmer-dev,
         golang-github-blevesearch-segment-dev,
         golang-github-boltdb-bolt-dev,
         golang-github-couchbase-moss-dev,
         golang-github-seiflotfy-cuckoofilter-dev,
         golang-github-spf13-cobra-dev,
         golang-github-steveyen-gtreap-dev,
         golang-github-willf-bitset-dev,
         golang-golang-x-net-dev,
         golang-golang-x-text-dev,
   	 golang-github-syndtr-goleveldb-dev,
         golang-goprotobuf-dev,
         golang-github-rcrowley-go-metrics-dev
Description: modern text indexing library for go
 Blevesearch is a library that provides full-text search and indexing
 for Go. It is designed to be fast, flexible, and easy to use.
 .
 Features:
   * Index any go data structure (including JSON)
   * Intelligent defaults backed up by powerful configuration
   * Supported field types:
     - Text
     - Numeric
     - Date
   * Supported query types:
     - Term, Phrase, Match, Match Phrase, Prefix
     - Conjunction, Disjunction, Boolean
     - Numeric Range, Date Range
     - Simple query syntax for human entry
   * tf-idf Scoring
   * Search result match highlighting
   * Supports Aggregating Facets:
     - Terms Facet
     - Numeric Range Facet
     - Date Range Facet
